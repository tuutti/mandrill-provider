<?php

namespace Tuutti\Mandrill\Provider;

use Pimple\Container;
use Pimple\ServiceProviderInterface;
use Mandrill;

class MandrillProvider implements ServiceProviderInterface {

  /**
   * @param Container $container
   */
  public function register(Container $app) {
    $app['mandrill'] = function($app) {
      if (isset($app['mandrill.password'])) {
        $mandrill = new Mandrill($app['mandrill.password']);
      }
      else {
        throw new \InvalidArgumentException("Error: Array key 'mandrill' does not exist or input data not Array");
      }
      return $mandrill;
    };
  }
}
